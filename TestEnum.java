package innogenttaskday3;

public class TestEnum {

	public static void main(String[] args) {

		Color red = Color.RED;
		System.out.println(red);

		String name = "BLUE";
		Color blue = Color.valueOf(name);
		System.out.println(blue);
		System.out.println(blue.toString());

		Color[] color = Color.values();// return all constant;
		for (Color colorType : color) {
			
			//order is important in enum
			System.out.println(colorType + "\t" + colorType.ordinal());
		}

	}

}
