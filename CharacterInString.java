package innogenttaskday3;

import java.util.HashMap;

public class CharacterInString {

	public static void main(String[] args) {

		String string = "onplan";
		HashMap<Character, Integer> countCharacter = new HashMap<>();

		for (int i = 0; i < string.length(); i++) {
			char key = string.charAt(i);

			if (countCharacter.containsKey(key)) {
				int value = countCharacter.get(key);

				countCharacter.put(key, ++value);

			} else {
				countCharacter.put(key, 1);
			}
		}
		countCharacter.forEach((character, frequency) -> System.out.println(character + "  " + frequency));

	}

}
