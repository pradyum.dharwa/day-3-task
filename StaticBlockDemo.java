package innogenttaskday3;

public class StaticBlockDemo {

	public static void main(String[] args) {

		System.out.println(InitailizeStatic.static_variable);
		System.out.println(InitializeFinalStatic.final_variable);

		// compile time constant show no static block called
		System.out.println(InitializeFinalStatic.final_variable);
//		System.out.println(12);

	}

}

class InitializeFinalStatic {

	static final int final_variable = 12;
	static {

//		final_variable=12;
		System.out.println("InitializeFinalStatic class static block called");
	}

}

class InitializeFinalStatic1 {

	static final int final_variable;
	static {

		final_variable = 120;
		System.out.println("InitializeFinalStatic class static block called");
	}

}

class InitailizeStatic {
	static int static_variable = 56;
	static {

		static_variable = 46;
		System.out.println("InitailizeFinale class static block called");
	}

}
