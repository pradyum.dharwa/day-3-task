package innogenttaskday3;

public class FinalVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassDemo class1 = new ClassDemo(30);
		System.out.println(class1.getMax_student());
		ClassDemo class2 = new ClassDemo(40);
		System.out.println(class2.getMax_student());

	}

}

final class ClassDemo {  // final class cannot inherited
	final int max_student;

	public ClassDemo(int max_student) {
		this.max_student = max_student;  //fixed than cannot changed  
	}

	public final int getMax_student() { // final method cannot override
		return max_student;
	}

}
