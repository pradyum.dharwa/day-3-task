package innogenttaskday3;

public class ExceptionMain {

	public static void main(String[] args) {
		String age = "17";// from user age is in String type;
		try {
			int age1 = Integer.parseInt(age);
			vote(age1);
		} catch (MyException myexception) {

			System.out.println("Age must be greater than equal to 18");
			System.out.println(myexception.getMessage());
		} catch (NumberFormatException numberFormatException) {

			System.out.println(numberFormatException.getMessage());
		} catch (Exception exception) {

			System.out.println("Excepton ==" + exception);
		} finally {
			System.out.println("all resources closed ");
		}

	}

	static public void vote(int age) {

		if (age >= 18) {
			System.out.println("welcome");
		} else {
			throw new MyException("You are not eligible , age =  " + age);
		}

	}

}
